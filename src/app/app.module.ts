import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// se agregó estas 4 línea
import { HttpClientModule } from '@angular/common/http';
import { ConversorComponent } from './conversor/conversor.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';


const appRoutes: Routes =[
  {
    path: 'conversion',
    //se ajusta con el valor del import de la linea 8
    component: ConversorComponent,
    data: {title: 'Conversor'}

  }
]

@NgModule({
  declarations: [
    AppComponent,
    ConversorComponent
  ],
  imports: [
    //se agragan estas dos lineas
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    //Se agregó esta linea
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
