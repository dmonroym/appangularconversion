import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

// remplazo por la URL de AWS
const endpoint = 'https://speu4pbi9h.execute-api.us-east-2.amazonaws.com/production/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) {}

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  //conversion es el parámetro retornado
  ConvertionForce (conversion): Observable<any> {
    console.log(conversion);
    //URL de la api en aws
    return this.http.post<any>(endpoint + 'forceConvert', JSON.stringify(conversion), httpOptions).pipe(
      //devuelve los datos por la consola del navegador de un valor que no se ha ingresado
      //tap((suma) => console.log(`la suma es = ${suma.data}`)),
      //Hace la observación para saber sobre cual hace la captura de excepciones
      catchError(this.handleError<any>('ConvertionForce'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}