import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-conversor',
  templateUrl: './conversor.component.html',
  styleUrls: ['./conversor.component.css']
})
export class ConversorComponent implements OnInit {

  //defines lo enviado
  datosSend = { unidadDato:"", unidadFinal:"", dato:"" };
  //recibo cualquier cosa
  datoReceive : any;

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }
  
  //metodo que ejecuta el componente (logica del front)
  // la idea es conecatr al post

  //Este metodo lo llama el conversor.component.html en el botón
  Convertir() {
    //configuracion del post
    this.rest.ConvertionForce(this.datosSend).subscribe((result) => {
     //traer la respuesta
      this.datoReceive = result;
      console.log(this.datoReceive);
      //this.router.navigate(['/product-details/'+result._id]);
    }, (err) => {
      console.log(err);
    });
    
  }
}